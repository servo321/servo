/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servo;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author UNIVASF
 */
@Entity
@Table(name = "atividade", catalog = "servo", schema = "")
@NamedQueries({
    @NamedQuery(name = "Atividade_1.findAll", query = "SELECT a FROM Atividade_1 a")
    , @NamedQuery(name = "Atividade_1.findByCod", query = "SELECT a FROM Atividade_1 a WHERE a.cod = :cod")
    , @NamedQuery(name = "Atividade_1.findByTitulo", query = "SELECT a FROM Atividade_1 a WHERE a.titulo = :titulo")
    , @NamedQuery(name = "Atividade_1.findByCategoria", query = "SELECT a FROM Atividade_1 a WHERE a.categoria = :categoria")
    , @NamedQuery(name = "Atividade_1.findByDescricao", query = "SELECT a FROM Atividade_1 a WHERE a.descricao = :descricao")})
public class Atividade_1 implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cod")
    private Integer cod;
    @Basic(optional = false)
    @Column(name = "titulo")
    private String titulo;
    @Basic(optional = false)
    @Column(name = "categoria")
    private String categoria;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;

    public Atividade_1() {
    }

    public Atividade_1(Integer cod) {
        this.cod = cod;
    }

    public Atividade_1(Integer cod, String titulo, String categoria, String descricao) {
        this.cod = cod;
        this.titulo = titulo;
        this.categoria = categoria;
        this.descricao = descricao;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        Integer oldCod = this.cod;
        this.cod = cod;
        changeSupport.firePropertyChange("cod", oldCod, cod);
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        String oldTitulo = this.titulo;
        this.titulo = titulo;
        changeSupport.firePropertyChange("titulo", oldTitulo, titulo);
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        String oldCategoria = this.categoria;
        this.categoria = categoria;
        changeSupport.firePropertyChange("categoria", oldCategoria, categoria);
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        String oldDescricao = this.descricao;
        this.descricao = descricao;
        changeSupport.firePropertyChange("descricao", oldDescricao, descricao);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cod != null ? cod.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Atividade_1)) {
            return false;
        }
        Atividade_1 other = (Atividade_1) object;
        if ((this.cod == null && other.cod != null) || (this.cod != null && !this.cod.equals(other.cod))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "servo.Atividade_1[ cod=" + cod + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
